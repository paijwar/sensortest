package com.paijwar.sensortest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();

    ProximitySensorManager mManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ProximitySensorManager.initSensor(this);
        mManager = ProximitySensorManager.getInstance();
        mManager.setSensorListener(tooClose -> {
            if (tooClose) {
                Toast.makeText(MainActivity.this, "Too close", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mManager.stopListening();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mManager.startListening();
    }
}
