package com.paijwar.sensortest;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class ProximitySensorManager {
    private static final ProximitySensorManager ourInstance = new ProximitySensorManager();

    public static ProximitySensorManager getInstance() {
        return ourInstance;
    }

    private Sensor proximitySensor;
    private SensorManager sensorManager;
    private WeakReference<ProximitySensorListener> listenerRef;
    private Context mContext = null;

    public static void initSensor(Context context) {
        ProximitySensorManager instance = getInstance();
        instance.mContext = context;

        instance.sensorManager = (SensorManager) instance.mContext.getSystemService(Context.SENSOR_SERVICE);
        instance.proximitySensor = instance.sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        if (instance.proximitySensor == null) {
            Toast.makeText(instance.mContext, "Proximity sensor not available", Toast.LENGTH_LONG).show();
            return;
        }
        // Register it, specifying the polling interval in microseconds
        instance.sensorManager.registerListener(instance.proximitySensorListener,
                instance.proximitySensor, 2 * 1000 * 1000);
    }

    public void setSensorListener(ProximitySensorListener listener) {
        listenerRef = new WeakReference<>(listener);
    }

    private boolean bIsListening = false;

    public void startListening() {
        if (!bIsListening) {
            sensorManager.registerListener(proximitySensorListener,
                    proximitySensor, 2 * 1000 * 1000);
            bIsListening = true;
        }
    }

    public void stopListening() {
        if (bIsListening) {
            if (proximitySensor != null) {
                sensorManager.unregisterListener(proximitySensorListener);
            }
            bIsListening = false;
        }
    }

    private ProximitySensorManager() {
    }

    interface ProximitySensorListener {
        void onProximityChanged(boolean tooClose);
    }

    private SensorEventListener proximitySensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {


            if (sensorEvent.values[0] < proximitySensor.getMaximumRange()) {
                if (listenerRef.get() != null) {
                    listenerRef.get().onProximityChanged(true);
                }
            } else {
                if (listenerRef.get() != null) {
                    listenerRef.get().onProximityChanged(false);
                }
            }

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };
}
